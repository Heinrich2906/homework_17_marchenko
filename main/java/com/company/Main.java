package com.company;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by heinr on 21.11.2016.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        //Таблица customer

//        CustomersOperations.selectAllCustomers();
//
//        Connection connection = getConnection();
//
//        ArrayList<Long> arrayListId = (ArrayList<Long>) CustomersOperations.selectCustomersId(connection);
//
//        for (Long s : arrayListId) {
//            System.out.println(s.toString());
//        }
//
//        CustomersOperations.selectCustomersByPassport("772465");
//
//        CustomersOperations.constantInsert();
//
//        CustomersOperations.selectAllCustomers();
//
//        CustomersOperations.updateCustomerFirstAndLastName(8L,"Elena", "Zinitch");
//
//        CustomersOperations.deleteCustomer(8L);
//
//        CustomersOperations.selectAllCustomers();

        //Таблица accounts

//        AccountsOperations.selectAccounts();
//
//        Connection connection = getConnection();
//
//        ArrayList<Long> arrayListAccounts = (ArrayList<Long>) AccountsOperations.selectAccountsId(connection);
//
//        for (Long s : arrayListAccounts) {
//            System.out.println(s.toString());
//        }
//
//        AccountsOperations.smartInsert(BigDecimal.valueOf(29.06), LocalDateTime.now().toString().replace("T"," "),"UAH",true,3L);
//
//        AccountsOperations.batchAccountsInsert();

        //Таблица transactions

        //TransactionsOperations.batchTransactionsInsert();
        //TransactionsOperations.updateAllTransactions();
        TransactionsOperations.deleteAllWithdraw();

    }

    public static Connection getConnection() throws Exception {

        Connection connection = null;

        Properties info = new Properties();
        info.put("user", "postgres");
        info.put("password", "admin");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", info);

        return connection;
    }

}
