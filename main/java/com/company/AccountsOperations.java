package com.company;

import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by heinr on 24.11.2016.
 */
public class AccountsOperations {

    //выводит информацию о customer в консоль (в одной строке)
    private static void printAccount(ResultSet result) throws SQLException {

        Long accauntNumber = result.getLong("accountNumber");
        BigDecimal balance = result.getBigDecimal("balance");
        Timestamp creationDate = result.getTimestamp("creationDate");
        String currency = result.getString("currency");
        boolean blocked = result.getBoolean("blocked");
        Long customerId = result.getLong("customerId");

        System.out.println(accauntNumber + " " + balance + " " + creationDate.toString() + " " + currency + " " + blocked + " " + customerId);
    }

    //выбирает все записи из таблицы "accounts" и выводит их в консоль
    public static void selectAccounts() throws SQLException{

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement();

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accounts");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();
                printAccount(resultSet);
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //возвращает список айдишников аккаунтов
    public static List<Long> selectAccountsId(Connection connection){
        List<Long> result = new ArrayList<Long>();

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            //connection - входящий параметр

            //2. Создание запроса
            Statement statement = connection.createStatement();

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT accountNumber FROM accounts");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();
                result.add(resultSet.getLong("accountNumber"));
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    //вставляет аккаунт с переданными данными
    public static void smartInsert(BigDecimal balance,String creationDate,String currency,boolean blocked,Long customerId){

        Connection connection = null;

        try {

            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("INSERT INTO accounts (balance, creationDate, currency, blocked, customerId) VALUES(?,?,?,?,?)");
            statement.setBigDecimal(1, balance);
            statement.setTimestamp(2, Timestamp.valueOf(creationDate));
            statement.setString(3, currency);
            statement.setBoolean(4, blocked);
            statement.setLong(5, customerId);

            //3. Выполнение запроса
            int updateCount = statement.executeUpdate();

            //4. Обработка результата запроса
//            while (!resultSet.isLast()) {
//                resultSet.next();
//                printCustomer(resultSet);
//            }

            // Шаг 5.1. Закрыть ResultSet
            //resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }    }

    public static void batchAccountsInsert(){

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();


            //2. Создание запроса
            Statement statement = connection.createStatement();
            PreparedStatement statementInsert = connection.prepareStatement("INSERT INTO accounts (balance, creationDate, currency, blocked, customerId) values(0,?,'UAH',true,?)");

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT id FROM customers");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();

                statementInsert.setTimestamp(1,Timestamp.valueOf(LocalDateTime.now()));
                statementInsert.setLong(2,resultSet.getLong("id"));
                statementInsert.addBatch();
            }

            statementInsert.executeBatch();
            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();
            statementInsert.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();                                    }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
