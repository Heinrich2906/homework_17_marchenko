package com.company;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Properties;

/**
 * Created by heinr on 26.11.2016.
 */
public class TransactionsOperations {

    //добавить для каждого счёта запись о внесении ('PUT') 100 денежных единиц, время - текущее
    public static void batchTransactionsInsert() {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement();
            PreparedStatement statementInsert = connection.prepareStatement("INSERT INTO transactions (amount, Date, operationType, accountNumber) VALUES(100,?,'PUT',?)");

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT accountNumber FROM accounts");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();

                statementInsert.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
                statementInsert.setLong(2, resultSet.getLong("accountNumber"));
                statementInsert.addBatch();
            }
            statementInsert.executeBatch();
            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();
            statementInsert.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void updateAllTransactions() {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT amount, id FROM transactions WHERE operationType = 'PUT'");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();

                // Обновляем balance
                resultSet.updateBigDecimal("amount", BigDecimal.valueOf(150.00));
                //resultSet.cancelRowUpdates();
                // Записываем изменения в базу
                resultSet.updateRow();

            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();


        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteAllWithdraw() {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            PreparedStatement statementInsert = connection.prepareStatement("INSERT INTO transactions (amount, date, operationtype, accountNumber) VALUES(100,?,'PUT',?)");

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT accountNumber, id FROM transactions WHERE operationType = 'WSD'");

            //Список уникальных accountNumber
            HashSet<Long> idHashSet = new HashSet<>();

            //4. Обработка результата запроса
            while ((!resultSet.isLast()) && (resultSet.next())) {

                idHashSet.add(resultSet.getLong("accountNumber"));

                // Удаляю строку
                resultSet.deleteRow();
            }

            for (Long accountNumber : idHashSet) {

                statementInsert.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
                statementInsert.setLong(2, accountNumber);
                statementInsert.addBatch();
            }

            statementInsert.executeBatch();

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();


        } catch (SQLException e) {
            e.printStackTrace();
            // } catch (ClassNotFoundException e) {
            //     e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
