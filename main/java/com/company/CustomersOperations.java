package com.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by heinr on 24.11.2016.
 */
public class CustomersOperations {

    //выводит информацию о customer в консоль (в одной строке)
    private static void printCustomer(ResultSet result) throws SQLException {

        Long id = result.getLong("id");
        String firstName = result.getString("firstName");
        String lastName = result.getString("lastName");
        Date birthDay = result.getDate("birthDay");
        String address = result.getString("address");
        String city = result.getString("city");
        String passport = result.getString("passport");
        String phone = result.getString("phone");

        System.out.println(id + " " + firstName + " " + lastName + " " + birthDay + " " + address + " " + city + " " + passport + " " + phone);

    }

    //выбирает все записи из таблицы
    public static void selectAllCustomers() throws Exception {

        Connection connection = null;

        try {

            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement();

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT * FROM customers");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();
                printCustomer(resultSet);
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //возвращает список id customer
    public static List<Long> selectCustomersId(Connection connection) throws Exception {

        List<Long> result = new ArrayList<Long>();

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            //connection - входящий параметр

            //2. Создание запроса
            Statement statement = connection.createStatement();

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT id FROM customers");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();
                result.add(resultSet.getLong("id"));
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    //выводит в консоль customer, паспорт которого равен переданному
    public static void selectCustomersByPassport(String passport) throws Exception {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers WHERE passport=?");
            statement.setString(1, passport);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();
                printCustomer(resultSet);
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //вставляет customer с одними и теми данными
    public static void constantInsert() throws Exception {
        smartInsert("Elena", "Zinich", "1978-05-13", "Vodnij per., build. 3-A", "Zaporozheye", "9388841", "0612675576");
    }

    //вставляет customer с переданными данными
    public static void smartInsert(String firstName, String lastName, String birthday, String address, String
            city, String passport, String phone) throws Exception {

        Connection connection = null;

        try {

            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("INSERT INTO customers (firstName, lastName, birthday, address, city, passport, phone) VALUES(?,?,?,?,?,?,?)");
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setDate(3, java.sql.Date.valueOf(birthday));
            statement.setString(4, address);
            statement.setString(5, city);
            statement.setString(6, passport);
            statement.setString(7, phone);

            //3. Выполнение запроса
            int updateCount = statement.executeUpdate();

            //4. Обработка результата запроса
//            while (!resultSet.isLast()) {
//                resultSet.next();
//                printCustomer(resultSet);
//            }

            // Шаг 5.1. Закрыть ResultSet
//            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //обновляет имя и фамилию customer с заданным id
    public static void updateCustomerFirstAndLastName(Long id, String firstName, String lastName) throws Exception {

        Connection connection = null;

        try {

            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("UPDATE customers SET firstName = ?, lastName = ? Where id = ?");
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setLong(3, id);

            //3. Выполнение запроса
            int updateCount = statement.executeUpdate();

            //4. Обработка результата запроса
//            while (!resultSet.isLast()) {
//                resultSet.next();
//                printCustomer(resultSet);
//            }

//            // Шаг 5.1. Закрыть ResultSet
//            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //удаляет customer в заданным id
    public static void deleteCustomer(Long id) throws Exception {

        Connection connection = null;

        try {

            //1. Зарегистрировать драйвер БД в менеджере
            connection = Main.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("DELETE FROM customers WHERE id = ?");
            statement.setLong(1, id);

            //3. Выполнение запроса
            int updateCount = statement.executeUpdate();

            //4. Обработка результата запроса
//            while (!resultSet.isLast()) {
//                resultSet.next();
//                printCustomer(resultSet);
//            }

//            // Шаг 5.1. Закрыть ResultSet
//            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
